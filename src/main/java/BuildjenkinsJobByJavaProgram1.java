import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class BuildjenkinsJobByJavaProgram1 {
    public static void main(String []args) throws IOException, InterruptedException {
        String url = "http://localhost:8080/job/TestJob/build?token=11ab4558afaccbd01b9bf61e01f60cc114";

        URL obj = new URL(url);

        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

        Thread.sleep(4000);

        System.out.println("Done - "+ connection.getResponseCode());

        connection.disconnect();
    }
}
