
import com.google.common.base.Optional;
import com.offbytwo.jenkins.*;
import com.offbytwo.jenkins.model.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Jenkins {

    // yaha se padha hai documentation wala part = "https://github.com/jenkinsci/java-client-api#getting-started"

    private static final String URL = "http://localhost:8080/";
    private static final String username = "himanshujhawar";
    private static final String password = "116bfb97b409531095ba3a0a670358fa6b";

    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {

//        System.out.println("Hi !!");
//        System.out.println("In The Documentation Part Method :- ");
//        documentationPart();

//        System.out.println("Hi !!");
//        System.out.println("In The BuildWithParam Method :- ");
//        BuildWithParam();

//        System.out.println();
//        System.out.println("In Test 1 :- ");
//        doesn't works , because , it is searching in URL which does not exist
//        test1();

//        System.out.println();
//        System.out.println("In Test 2 :- ");
//        test2();

        System.out.println("Hi !!");
        System.out.println("In The BuildJavaProgram Part Method :- ");
        BuildJavaProgram();
    }

    // Job Details , Building job , Creating or Deleting Job/Folder , BuildHistory of Job
    public static void documentationPart() throws URISyntaxException, IOException, InterruptedException {
        JenkinsServer jenkins = new JenkinsServer(new URI("http://localhost:8080/") , username , password);

//        try {
//            // if folder already exists/deleted then put exception in catch statement
//            jenkins.deleteJob("FolderOpen");
//        } catch(Exception e) {
//        }

        // getting all jobs
        Map<String, Job> jobs = jenkins.getJobs();

        for(String nameOfJob : jobs.keySet())
        {
//            String nameOfJob = "SlackNotifications";
            // details of particular job
            JobWithDetails job = jobs.get(nameOfJob).details();
            System.out.println(nameOfJob + " = " + job);

            // URL of the job
            System.out.println("URL of " + job.getDisplayName() + " -> " + job.getUrl());

            // detect if job is folder or not
            boolean isBuildable = job.isBuildable();
            if(isBuildable==false) continue;

            // build history of this job
            List<Build> buildHistory = job.getAllBuilds();
            System.out.println("buildHistory = " + buildHistory);

            // each build details
            for(Build build : buildHistory)
            {
                BuildWithDetails details = build.details();
                System.out.println(details.getFullDisplayName() + " , " + details.getDuration() + " , ");
            }

            // build job
            JenkinsTriggerHelper buildJob = new JenkinsTriggerHelper(jenkins);
            BuildWithDetails bwd = buildJob.triggerJobAndWaitUntilFinished(nameOfJob);
            System.out.println(bwd.getResult());
        }

    }

    // building job inside of the folder
    public static void BuildWithParam() throws URISyntaxException, IOException, InterruptedException {
        JenkinsServer jenkins = new JenkinsServer(new URI(URL) , username , password);
        Map<String,Job> jobs = jenkins.getJobs();

        // getting FolderJob
        Optional<FolderJob> folder = jenkins.getFolderJob(jobs.get("Folder1"));
        System.out.println(folder);

        // Details of Job inside the Folder
        JobWithDetails myJob = jenkins.getJob(folder.get() , "BuildWithParameters1");
        System.out.println(myJob.getName()+" --> "+myJob.getUrl());

        // URL of the Folder
        String folderURL = jenkins.getJob("Folder1").getUrl();
        JenkinsServer jenkins2 = new JenkinsServer(new URI(folderURL) , username , password);

        // initialising parameters
        Map<String,String> params = new HashMap<>();
        params.put("one","1");
        params.put("two","2");
        params.put("three","4");

        // building Job inside of the Folder
        JenkinsTriggerHelper build = new JenkinsTriggerHelper(jenkins2);
        BuildWithDetails bwd = build.triggerJobAndWaitUntilFinished(myJob.getName() , params);
//        System.out.println(bwd.isBuilding());
//        System.out.println(bwd.getResult());
    }

    // building java program from jenkins build
    public static void BuildJavaProgram() throws URISyntaxException, IOException, InterruptedException {
        JenkinsServer jenkins = new JenkinsServer(new URI("http://localhost:8080/") , username , password);
        Job job = jenkins.getJobs().get("BuildJavaProgram");

        JenkinsTriggerHelper buildJob = new JenkinsTriggerHelper(jenkins);
        BuildWithDetails bwd = buildJob.triggerJobAndWaitUntilFinished(job.getName());
        System.out.println(bwd.getResult());
    }

    public static void test1() throws IOException, URISyntaxException {
        JenkinsServer jenkins = new JenkinsServer(new URI(URL) );
        Map<String, Job> jobs = jenkins.getJobs();
        JobWithDetails job = jobs.get("Pipeline").details();

        Build lastCompletedBuild = job.getLastCompletedBuild();

        TestReport testReport = lastCompletedBuild.getTestReport();
        System.out.println(" --- TestReport ---");

        System.out.println("totalCount: " + testReport.getTotalCount());
        System.out.println(" failCount: " + testReport.getFailCount());
        System.out.println(" skipCount: " + testReport.getSkipCount());

        TestResult testResult = lastCompletedBuild.getTestResult();
        System.out.println(" --- TestResult ---");

        System.out.println(" PassCount: " + testResult.getPassCount());
        System.out.println(" failCount: " + testResult.getFailCount());
        System.out.println(" skipCount: " + testResult.getSkipCount());
        System.out.println("  duration: " + testResult.getDuration());
        System.out.println("   isEmpty: " + testResult.isEmpty());
    }


    public static void test2() throws IOException {
        JenkinsServer js = new JenkinsServer( URI.create(URL) , username , password );
        Map<String, View> views = js.getViews();
        for (Map.Entry<String, View> item : views.entrySet()) {
            String viewName = item.getKey();
            View value = item.getValue();
            System.out.println("URL of " + viewName + " : " + value.getUrl());
        }
//        for (String item : views.keySet()) {
//            View value = views.get(item);
//            System.out.println("URL: " + value.getUrl());
//        }

    }

}
